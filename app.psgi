#!/usr/bin/env perl

use strict;
use warnings;
use utf8;
use Plack::Builder;


my $app = sub {
  my $env = shift;
};

$app = builder {
  enable 'Static',
    path => qr!^/!,
    root => 'static/';
  $app;
};

