"use strict";

var gulp       = require('gulp');
var browserify = require('browserify');
var babelify   = require('babelify');
var source     = require('vinyl-source-stream');


gulp.task('browserify', function() {
  browserify('./src/app.js', {debug: true})
    .transform(babelify)
    .bundle()
    .on("error", function(err) {
      console.log("Error : " + err.message);
    })
    .pipe(source('app.js'))
    .pipe(gulp.dest('./static'));
});

gulp.task('watch', function () {
  gulp.watch('src/**/*.js', ['browserify']);
});

//gulp.task 'webserver', ->
//  gulp.src("app")
//  .pipe(webserver({
//    port: 8000
//    livereload: true
//    directoryListing: false
//  }))

gulp.task('default', ['browserify', 'watch']);