(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var Hello = require("./class/Hello");
var AspectRatioScreen = require("./class/AspectRatioScreen");

new Hello();

var ars = new AspectRatioScreen({
  screen: ".container",
  fit: "> iframe",
  aspect: [16, 9]
});

var timerId = 0;
window.addEventListener("resize", function () {
  clearTimeout(timerId);
  timerId = setTimeout(function () {
    ars.fitScreen();
  }, 100);
}, false);

},{"./class/AspectRatioScreen":2,"./class/Hello":3}],2:[function(require,module,exports){
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AspectRatioScreen = (function () {
  function AspectRatioScreen(configs) {
    _classCallCheck(this, AspectRatioScreen);

    // requires
    this._screen = document.querySelector(configs.screen);
    this._fit = document.querySelector(configs.screen + configs.fit);
    this._aspect = configs && configs.aspect || [4, 3];

    this.fitScreen();
  }

  /**
   * View の height と width を返す
   * @returns {*[]}
   */

  _createClass(AspectRatioScreen, [{
    key: "getScreenBox",
    value: function getScreenBox() {
      return {
        width: this._screen.clientWidth,
        height: this._screen.clientHeight
      };
    }

    /**
     * @returns {string}
     */
  }, {
    key: "fitScreen",
    value: function fitScreen() {
      var newWidth, newHeight;
      var offset = 0;

      // get screen width and screen height
      var box = this.getScreenBox();

      // height に合わせて width を計算する
      newWidth = this.getAspectWidth(box.height, this._aspect);

      if (box.width > newWidth) {
        // width 揃え
        newHeight = this.getAspectHeight(box.width, this._aspect);
        offset = ~ ~((box.height - newHeight) / 2);

        this.setStyles(box.width, newHeight, null, offset);
      } else {
        // height 揃え
        offset = ~ ~((box.width - newWidth) / 2);

        this.setStyles(newWidth, box.height, offset, null);
      }
    }
  }, {
    key: "setStyles",
    value: function setStyles(width, height, offsetLeft, offsetTop) {
      this._fit.removeAttribute("style");

      this._fit.style.width = width + "px";
      this._fit.style.height = height + "px";

      if (null != offsetLeft) {
        this._fit.style.marginLeft = offsetLeft + "px";
      } else if (null != offsetTop) {
        this._fit.style.marginTop = offsetTop + "px";
      }
    }

    /**
     * height を元に アスペクト比に対応した width を返す
     * @param height
     * @param aspect
     * @returns {number}
     */
  }, {
    key: "getAspectWidth",
    value: function getAspectWidth(height, aspect) {
      return ~ ~(height * (aspect[0] / aspect[1])); // height to aspect width
    }

    /**
     * width を元に アスペクト比に対応した height を返す
     * @param width
     * @param aspect
     * @returns {number}
     */
  }, {
    key: "getAspectHeight",
    value: function getAspectHeight(width, aspect) {
      return ~ ~(width / (aspect[0] / aspect[1])); // width to aspect height
    }
  }]);

  return AspectRatioScreen;
})();

module.exports = AspectRatioScreen;

},{}],3:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Hello = function Hello() {
  _classCallCheck(this, Hello);

  console.log("hello");
};

module.exports = Hello;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMvc2FrdXlhc3Vnby9EZXNrdG9wL3BsYXlncm91bmQvc3JjL2FwcC5qcyIsIi9Vc2Vycy9zYWt1eWFzdWdvL0Rlc2t0b3AvcGxheWdyb3VuZC9zcmMvY2xhc3MvQXNwZWN0UmF0aW9TY3JlZW4uanMiLCIvVXNlcnMvc2FrdXlhc3Vnby9EZXNrdG9wL3BsYXlncm91bmQvc3JjL2NsYXNzL0hlbGxvLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDckMsSUFBSSxpQkFBaUIsR0FBRyxPQUFPLENBQUMsMkJBQTJCLENBQUMsQ0FBQzs7QUFFN0QsSUFBSSxLQUFLLEVBQUUsQ0FBQzs7QUFFWixJQUFJLEdBQUcsR0FBRyxJQUFJLGlCQUFpQixDQUFDO0FBQzlCLFFBQU0sRUFBRSxZQUFZO0FBQ3BCLEtBQUcsRUFBRSxVQUFVO0FBQ2YsUUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztDQUNoQixDQUFDLENBQUM7O0FBRUgsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDO0FBQ2hCLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsWUFBVztBQUMzQyxjQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDdEIsU0FBTyxHQUFHLFVBQVUsQ0FBQyxZQUFZO0FBQy9CLE9BQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztHQUNqQixFQUFFLEdBQUcsQ0FBQyxDQUFDO0NBQ1QsRUFBRSxLQUFLLENBQUMsQ0FBQzs7Ozs7Ozs7O0lDakJKLGlCQUFpQjtBQUNWLFdBRFAsaUJBQWlCLENBQ1QsT0FBTyxFQUFFOzBCQURqQixpQkFBaUI7OztBQUduQixRQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ3RELFFBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUNqRSxRQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sSUFBSSxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDOztBQUVuRCxRQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7R0FDbEI7Ozs7Ozs7ZUFSRyxpQkFBaUI7O1dBY1Qsd0JBQUc7QUFDYixhQUFPO0FBQ0wsYUFBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVztBQUMvQixjQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZO09BQ2xDLENBQUM7S0FDSDs7Ozs7OztXQUtRLHFCQUFHO0FBQ1YsVUFBSSxRQUFRLEVBQUUsU0FBUyxDQUFDO0FBQ3hCLFVBQUksTUFBTSxHQUFHLENBQUMsQ0FBQzs7O0FBR2YsVUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDOzs7QUFHOUIsY0FBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7O0FBRXpELFVBQUksR0FBRyxDQUFDLEtBQUssR0FBRyxRQUFRLEVBQUU7O0FBRXhCLGlCQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUMxRCxjQUFNLEdBQUcsRUFBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUEsR0FBSSxDQUFDLENBQUEsQUFBQyxDQUFDOztBQUUxQyxZQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztPQUVwRCxNQUFNOztBQUVMLGNBQU0sR0FBRyxFQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQSxHQUFJLENBQUMsQ0FBQSxBQUFDLENBQUM7O0FBRXhDLFlBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO09BQ3BEO0tBQ0Y7OztXQUVRLG1CQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRTtBQUM5QyxVQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7QUFFbkMsVUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUM7QUFDckMsVUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUM7O0FBRXZDLFVBQUksSUFBSSxJQUFJLFVBQVUsRUFBRTtBQUN0QixZQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsVUFBVSxHQUFHLElBQUksQ0FBQztPQUNoRCxNQUFNLElBQUksSUFBSSxJQUFJLFNBQVMsRUFBRTtBQUM1QixZQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsU0FBUyxHQUFHLElBQUksQ0FBQztPQUM5QztLQUNGOzs7Ozs7Ozs7O1dBUWEsd0JBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRTtBQUM3QixhQUFPLEVBQUMsRUFBRSxNQUFNLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQSxDQUFDLEFBQUMsQ0FBQztLQUM3Qzs7Ozs7Ozs7OztXQVFjLHlCQUFDLEtBQUssRUFBRSxNQUFNLEVBQUU7QUFDN0IsYUFBTyxFQUFDLEVBQUUsS0FBSyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUEsQ0FBQyxBQUFDLENBQUM7S0FDNUM7OztTQWhGRyxpQkFBaUI7OztBQW1GdkIsTUFBTSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQzs7O0FDbkZuQyxZQUFZLENBQUM7Ozs7SUFFUCxLQUFLLEdBQ0UsU0FEUCxLQUFLLEdBQ0s7d0JBRFYsS0FBSzs7QUFFUCxTQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0NBQ3RCOztBQUdILE1BQU0sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsInZhciBIZWxsbyA9IHJlcXVpcmUoXCIuL2NsYXNzL0hlbGxvXCIpO1xudmFyIEFzcGVjdFJhdGlvU2NyZWVuID0gcmVxdWlyZShcIi4vY2xhc3MvQXNwZWN0UmF0aW9TY3JlZW5cIik7XG5cbm5ldyBIZWxsbygpO1xuXG52YXIgYXJzID0gbmV3IEFzcGVjdFJhdGlvU2NyZWVuKHtcbiAgc2NyZWVuOiBcIi5jb250YWluZXJcIixcbiAgZml0OiBcIj4gaWZyYW1lXCIsXG4gIGFzcGVjdDogWzE2LCA5XVxufSk7XG5cbnZhciB0aW1lcklkID0gMDtcbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpe1xuICBjbGVhclRpbWVvdXQodGltZXJJZCk7XG4gIHRpbWVySWQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICBhcnMuZml0U2NyZWVuKCk7XG4gIH0sIDEwMCk7XG59LCBmYWxzZSk7XG4iLCJjbGFzcyBBc3BlY3RSYXRpb1NjcmVlbiB7XG4gIGNvbnN0cnVjdG9yKGNvbmZpZ3MpIHtcbiAgICAvLyByZXF1aXJlc1xuICAgIHRoaXMuX3NjcmVlbiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoY29uZmlncy5zY3JlZW4pO1xuICAgIHRoaXMuX2ZpdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoY29uZmlncy5zY3JlZW4gKyBjb25maWdzLmZpdCk7XG4gICAgdGhpcy5fYXNwZWN0ID0gY29uZmlncyAmJiBjb25maWdzLmFzcGVjdCB8fCBbNCwgM107XG5cbiAgICB0aGlzLmZpdFNjcmVlbigpO1xuICB9XG5cbiAgLyoqXG4gICAqIFZpZXcg44GuIGhlaWdodCDjgaggd2lkdGgg44KS6L+U44GZXG4gICAqIEByZXR1cm5zIHsqW119XG4gICAqL1xuICBnZXRTY3JlZW5Cb3goKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHdpZHRoOiB0aGlzLl9zY3JlZW4uY2xpZW50V2lkdGgsXG4gICAgICBoZWlnaHQ6IHRoaXMuX3NjcmVlbi5jbGllbnRIZWlnaHRcbiAgICB9O1xuICB9XG5cbiAgLyoqXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAqL1xuICBmaXRTY3JlZW4oKSB7XG4gICAgdmFyIG5ld1dpZHRoLCBuZXdIZWlnaHQ7XG4gICAgdmFyIG9mZnNldCA9IDA7XG5cbiAgICAgLy8gZ2V0IHNjcmVlbiB3aWR0aCBhbmQgc2NyZWVuIGhlaWdodFxuICAgIHZhciBib3ggPSB0aGlzLmdldFNjcmVlbkJveCgpO1xuXG4gICAgLy8gaGVpZ2h0IOOBq+WQiOOCj+OBm+OBpiB3aWR0aCDjgpLoqIjnrpfjgZnjgotcbiAgICBuZXdXaWR0aCA9IHRoaXMuZ2V0QXNwZWN0V2lkdGgoYm94LmhlaWdodCwgdGhpcy5fYXNwZWN0KTtcblxuICAgIGlmIChib3gud2lkdGggPiBuZXdXaWR0aCkge1xuICAgICAgLy8gd2lkdGgg5o+D44GIXG4gICAgICBuZXdIZWlnaHQgPSB0aGlzLmdldEFzcGVjdEhlaWdodChib3gud2lkdGgsIHRoaXMuX2FzcGVjdCk7XG4gICAgICBvZmZzZXQgPSB+figoYm94LmhlaWdodCAtIG5ld0hlaWdodCkgLyAyKTtcblxuICAgICAgdGhpcy5zZXRTdHlsZXMoYm94LndpZHRoLCBuZXdIZWlnaHQsIG51bGwsIG9mZnNldCk7XG5cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gaGVpZ2h0IOaPg+OBiFxuICAgICAgb2Zmc2V0ID0gfn4oKGJveC53aWR0aCAtIG5ld1dpZHRoKSAvIDIpO1xuXG4gICAgICB0aGlzLnNldFN0eWxlcyhuZXdXaWR0aCwgYm94LmhlaWdodCwgb2Zmc2V0LCBudWxsKTtcbiAgICB9XG4gIH1cblxuICBzZXRTdHlsZXMod2lkdGgsIGhlaWdodCwgb2Zmc2V0TGVmdCwgb2Zmc2V0VG9wKSB7XG4gICAgdGhpcy5fZml0LnJlbW92ZUF0dHJpYnV0ZShcInN0eWxlXCIpO1xuXG4gICAgdGhpcy5fZml0LnN0eWxlLndpZHRoID0gd2lkdGggKyBcInB4XCI7XG4gICAgdGhpcy5fZml0LnN0eWxlLmhlaWdodCA9IGhlaWdodCArIFwicHhcIjtcblxuICAgIGlmIChudWxsICE9IG9mZnNldExlZnQpIHtcbiAgICAgIHRoaXMuX2ZpdC5zdHlsZS5tYXJnaW5MZWZ0ID0gb2Zmc2V0TGVmdCArIFwicHhcIjtcbiAgICB9IGVsc2UgaWYgKG51bGwgIT0gb2Zmc2V0VG9wKSB7XG4gICAgICB0aGlzLl9maXQuc3R5bGUubWFyZ2luVG9wID0gb2Zmc2V0VG9wICsgXCJweFwiO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBoZWlnaHQg44KS5YWD44GrIOOCouOCueODmuOCr+ODiOavlOOBq+WvvuW/nOOBl+OBnyB3aWR0aCDjgpLov5TjgZlcbiAgICogQHBhcmFtIGhlaWdodFxuICAgKiBAcGFyYW0gYXNwZWN0XG4gICAqIEByZXR1cm5zIHtudW1iZXJ9XG4gICAqL1xuICBnZXRBc3BlY3RXaWR0aChoZWlnaHQsIGFzcGVjdCkge1xuICAgIHJldHVybiB+fihoZWlnaHQgKiAoYXNwZWN0WzBdIC8gYXNwZWN0WzFdKSk7IC8vIGhlaWdodCB0byBhc3BlY3Qgd2lkdGhcbiAgfVxuXG4gIC8qKlxuICAgKiB3aWR0aCDjgpLlhYPjgasg44Ki44K544Oa44Kv44OI5q+U44Gr5a++5b+c44GX44GfIGhlaWdodCDjgpLov5TjgZlcbiAgICogQHBhcmFtIHdpZHRoXG4gICAqIEBwYXJhbSBhc3BlY3RcbiAgICogQHJldHVybnMge251bWJlcn1cbiAgICovXG4gIGdldEFzcGVjdEhlaWdodCh3aWR0aCwgYXNwZWN0KSB7XG4gICAgcmV0dXJuIH5+KHdpZHRoIC8gKGFzcGVjdFswXSAvIGFzcGVjdFsxXSkpOyAvLyB3aWR0aCB0byBhc3BlY3QgaGVpZ2h0XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBBc3BlY3RSYXRpb1NjcmVlbjsiLCJcInVzZSBzdHJpY3RcIjtcblxuY2xhc3MgSGVsbG8ge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBjb25zb2xlLmxvZyhcImhlbGxvXCIpO1xuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gSGVsbG87Il19
