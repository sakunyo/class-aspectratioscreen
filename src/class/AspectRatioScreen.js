class AspectRatioScreen {
  constructor(configs) {
    // requires
    this._screen = document.querySelector(configs.screen);
    this._fit = document.querySelector(configs.screen + configs.fit);
    this._aspect = configs && configs.aspect || [4, 3];

    this.fitScreen();
  }

  /**
   * View の height と width を返す
   * @returns {*[]}
   */
  getScreenBox() {
    return {
      width: this._screen.clientWidth,
      height: this._screen.clientHeight
    };
  }

  /**
   * @returns {string}
   */
  fitScreen() {
    var newWidth, newHeight;
    var offset = 0;

     // get screen width and screen height
    var box = this.getScreenBox();

    // height に合わせて width を計算する
    newWidth = this.getAspectWidth(box.height, this._aspect);

    if (box.width > newWidth) {
      // width 揃え
      newHeight = this.getAspectHeight(box.width, this._aspect);
      offset = ~~((box.height - newHeight) / 2);

      this.setStyles(box.width, newHeight, null, offset);

    } else {
      // height 揃え
      offset = ~~((box.width - newWidth) / 2);

      this.setStyles(newWidth, box.height, offset, null);
    }
  }

  setStyles(width, height, offsetLeft, offsetTop) {
    this._fit.removeAttribute("style");

    this._fit.style.width = width + "px";
    this._fit.style.height = height + "px";

    if (null != offsetLeft) {
      this._fit.style.marginLeft = offsetLeft + "px";
    } else if (null != offsetTop) {
      this._fit.style.marginTop = offsetTop + "px";
    }
  }

  /**
   * height を元に アスペクト比に対応した width を返す
   * @param height
   * @param aspect
   * @returns {number}
   */
  getAspectWidth(height, aspect) {
    return ~~(height * (aspect[0] / aspect[1])); // height to aspect width
  }

  /**
   * width を元に アスペクト比に対応した height を返す
   * @param width
   * @param aspect
   * @returns {number}
   */
  getAspectHeight(width, aspect) {
    return ~~(width / (aspect[0] / aspect[1])); // width to aspect height
  }
}

module.exports = AspectRatioScreen;