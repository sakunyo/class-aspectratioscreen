var Hello = require("./class/Hello");
var AspectRatioScreen = require("./class/AspectRatioScreen");

new Hello();

var ars = new AspectRatioScreen({
  screen: ".container",
  fit: "> iframe",
  aspect: [16, 9]
});

var timerId = 0;
window.addEventListener("resize", function (){
  clearTimeout(timerId);
  timerId = setTimeout(function () {
    ars.fitScreen();
  }, 100);
}, false);
